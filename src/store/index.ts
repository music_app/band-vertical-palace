import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, createStore, Store} from 'redux';
import rootReducer from './ducks/rootReducer';
import rootSaga from './ducks/rootSaga';
import { BandsState } from './ducks/bands/types';

export interface ApplicationState {
    bands: BandsState
}

const sagaMiddleware = createSagaMiddleware();

const store: Store<ApplicationState> = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;
