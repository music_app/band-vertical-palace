import { all, takeLatest } from 'redux-saga/effects';

import { BandsTypes } from './bands/types';
import { loadBands } from './bands/sagas';

export default function* rootSaga() {
  return yield all([
    takeLatest(BandsTypes.LOAD_BANDS, loadBands),
  ]);
}
