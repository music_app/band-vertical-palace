import { Reducer } from 'redux';
import { BandsState, BandsTypes } from './types';

const INITIAL_STATE: BandsState = {
  data: [],
  loading: false,
  error: false,
};

const reducer: Reducer<BandsState> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case BandsTypes.LOAD_BANDS:
      return { ...state, loading: true };
    case BandsTypes.LOAD_BANDS_SUCCESS:
      return {
        ...state, loading: false, error: false, data: action.payload,
      };
    case BandsTypes.LOAD_BANDS_FAILURE:
      return {
        ...state, loading: false, error: true, data: [],
      };
    default:
      return state;
  }
};

export default reducer;
