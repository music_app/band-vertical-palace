
import { call, put } from 'redux-saga/effects';
import api from '../../../services/api';

import { loadBandsFailure, loadBandsSuccess } from './actions';

export function* loadBands() {
  try {
    const response = yield call(api.get, 'bands');

    yield put(loadBandsSuccess(response.data));
  } catch (err) {
    yield put(loadBandsFailure());
  }
}
