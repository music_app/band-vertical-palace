export enum BandsTypes {
  LOAD_BANDS = '@bands/LOAD_BANDS',
  LOAD_BANDS_SUCCESS = '@bands/LOAD_BANDS_SUCCESS',
  LOAD_BANDS_FAILURE = '@bands/LOAD_BANDS_FAILURE',
}

export enum Genre {
    HARDCORE,
    CLASSIC_ROCK,
    METAL
}

export interface Band {
    id: number,
    name: string,
    created: string,
    genre: Genre,
}

export interface BandsState{
    readonly data: Band[],
    readonly loading: boolean,
    readonly error: boolean
}
