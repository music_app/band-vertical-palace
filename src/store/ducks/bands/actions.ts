import { action } from 'typesafe-actions';
import { BandsTypes, Band } from './types';

export const loadBands = () => action(BandsTypes.LOAD_BANDS);

export const loadBandsSuccess = (data: Band[]) => action(BandsTypes.LOAD_BANDS_SUCCESS, data);

export const loadBandsFailure = () => action(BandsTypes.LOAD_BANDS_FAILURE);
