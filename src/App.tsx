import React from 'react';
import { Provider } from 'react-redux';

import BandList from './components/BandList';

import store from './store';

const App = () => <Provider store={store}><BandList /></Provider>;

export default App;
