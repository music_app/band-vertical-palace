import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { Band } from '../../store/ducks/bands/types';
import { ApplicationState } from '../../store';

import * as bandsActions from '../../store/ducks/bands/actions';

interface StateProps {
    bands: Band[]
}

interface DispatchProps {
    loadBands(): void
}

type Props = StateProps & DispatchProps

class BandList extends Component<Props> {
  componentDidMount() {
    const { loadBands } = this.props;

    loadBands();
  }

  render() {
    const { bands } = this.props;

    return (
      <ul>
        {bands.map((band) => (
          <li>{band.name}</li>
        ))}
      </ul>
    );
  }
}

const mapStateToProps = (state: ApplicationState) => ({
  bands: state.bands.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators(bandsActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BandList);
